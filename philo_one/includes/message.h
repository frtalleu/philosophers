/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   message.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/18 17:54:53 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/18 17:54:55 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _MESSAGE_H
# define _MESSAGE_H

static const char	*g_msg[5] =
{
	"died",
	"is thinking",
	"is eating",
	"is sleeping",
	"has taken a fork"
};

#endif
